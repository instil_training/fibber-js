import {fibber, recursive_fibber} from '../src/fibber'

describe("the fibber function", function() {

    it("should return 0 as the first number in the series", function() {
        let result = fibber(0);
        expect(result).toBe(0);
    });

    it("should return 1 as the second number in the series", function() {
        let result = fibber(1);
        expect(result).toBe(1);
    });

    it("should return 1 as the third number in the series", function() {
        let result = fibber(2);
        expect(result).toBe(1);
    });

    it("should return 55 as the tenth number in the series", function() {
        let result = fibber(10);
        expect(result).toBe(55);
    });

    it("should return -1 if passed a value that is not a number", function() {
        ["thing", null, undefined].forEach((val) => {
            let result = fibber(val);
            expect(result).toBe(-1);
        });
    });

    describe("recursive version", function() {

        it("should return the same value is the non-recursive version", function() {
            [0, 1, 25, null].forEach((val) => {
                let result = fibber(val);
                let recursive_result = recursive_fibber(val);
                expect(recursive_result).toEqual(result);
            })
        });

    });

});
