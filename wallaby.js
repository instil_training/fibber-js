module.exports = (wallaby) => {
    return {
        files: ['src/**/*.js'],
        tests: ['test/**/*.js'],
        testFramework: 'jasmine',
        env: {
            type: 'node'
        },
        compilers: {
            "**/*.js": wallaby.compilers.babel({
                presets: ['es2015']
            }),
        }
    }
}